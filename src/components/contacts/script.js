window.addEventListener('load', function (event) {
	let isMapInited = false;
	if (!isMapInited) {
		isMapInited = true;
		(async () => {
			await new Promise((resolve, reject) => {
				$.getScript({
					url: 'https://api-maps.yandex.ru/2.1/?apikey=AQAAAABYOCAVM1bfnx9nlHU_xs6Z_Sf_lK8k&lang=ru_RU',
					dataType: "script",
					success: () => resolve()
				})
			})

			ymaps.ready(init);


			function init() {
				// Создание карты.

				const [initLat, initLon] = $('.js-map').first().attr('data-geo').split(',');

				var myMap = new ymaps.Map("map", {
					center: [initLat, initLon],
					// от 0 (весь мир) до 19.
					zoom: 16
				});

				const places = $('.js-contact-item');

				const createPlacemark = (lat, lon) => {
					var myPlacemark = new ymaps.Placemark([lat, lon], {}, {
						balloonShadow: false,
						balloonPanelMaxMapArea: 0,
						balloonOffset: [0, -72],
						iconLayout: 'default#image',
						// Своё изображение иконки метки.
						iconImageHref: '/local/templates/html/images/map-mark.svg',
						// Размеры метки.
						iconImageSize: [76, 82],
						// Смещение левого верхнего угла иконки относительно
						// её "ножки" (точки привязки).
						iconImageOffset: [-38, -82]
					});

					return myPlacemark
				}



				myMap.geoObjects.add(createPlacemark(initLat, initLon));
			}
		})()
	}
})