window.addEventListener('load', function () {

	if (!window.reinit) {
		window.reinit = {};
	}
	if (!window.reinit.slider) {
		window.reinit.slider = {};
	}

	window.reinit.slider.lineSlider = function () {
		let lineSlider = $('.js-line-slider')
		if (lineSlider.length && !lineSlider.hasClass('inited')) {
			lineSlider.each(function (i, el) {


				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						autoHeight: false,
						lazy: true,
						spaceBetween: 0,
						watchOverflow: true,
						simulateTouch: true,
						slidesPerView: 'auto',
						watchSlidesVisibility: true,
						loop: false,
						// effect: 'fade',
						pagination: {
							el: $(el).find('.js-swiper-pagination'),
							clickable: true
						},
						fadeEffect: {
							crossFade: true
						},
						on: {
							slideChange: function () {
							}
						},

					}
				})
					.on('init', () => {

					})
			});
			lineSlider.addClass('inited')
		}
	};

	window.reinit.slider.lineSlider();
})