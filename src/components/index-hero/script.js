window.addEventListener('load', function () {
	if (!window.reinit) {
		window.reinit = {};
	}
	if (!window.reinit.slider) {
		window.reinit.slider = {};
	}

	window.reinit.slider.fsslider = function () {
		let fsSlider = $('.js-slider-fs')
		if (fsSlider.length && !fsSlider.hasClass('inited')) {
			fsSlider.each(function (i, el) {



				let nav = undefined;



				if ($(window).width() > 800) {

					nav = window.slam_slider({
						el: $('.js-slider-fs-nav'),
						args: {
							lazy: true,
							spaceBetween: 16,
							slidesPerView: 'auto',
						}
					})
				}




				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						autoHeight: false,
						lazy: true,
						spaceBetween: 0,
						watchOverflow: true,
						simulateTouch: true,
						slidesPerView: 'auto',
						// loop: length > 1,
						// effect: 'fade',
						pagination: {
							clickable: true,
							el: $(el).find('.js-swiper-pagination')
						},
						fadeEffect: {
							crossFade: true
						},
						on: {
							slideChange: function () {
								let position = 0;
								setTimeout(() => {
									$('.slider-fs .swiper-wrapper .swiper-slide').each(function (index) {
										if ($(this).hasClass('swiper-slide-active')) {
											position = index;
											return;
										}
									})
									nav.slideTo(position);
								}, 0)


							}
						},
						thumbs: { swiper: nav }
					}
				})
					.on('init', () => {
						// swiper.autoplay.delay = 5000;
						// setTimeout(function(){
						// 	swiper.autoplay.start()
						// }, 3000);
					})
			});
			fsSlider.addClass('inited')
		}
	};

	window.reinit.slider.fsslider();



	window.reinit.slider.productSlider = function () {
		let productSlider = $('.js-slider-product-hero')
		if (productSlider.length && !productSlider.hasClass('inited')) {
			productSlider.each(function (i, el) {

				let nav = undefined;

				if ($(window).width() > 800) {
					if ($('.js-slider-product-nav').length) {
						nav = window.slam_slider({
							el: $('.js-slider-product-nav'),
							args: {
								lazy: true,
								spaceBetween: 16,
								slidesPerView: 6,
							}
						}
						)
					}

				}

				const thumbs = nav ? { thumbs: { swiper: nav } } : {};

				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						autoHeight: false,
						lazy: true,
						spaceBetween: 50,
						watchOverflow: true,
						simulateTouch: true,
						slidesPerView: 'auto',
						// loop: length > 1,
						// effect: 'fade',
						pagination: {
							clickable: true,
							el: $(el).find('.js-swiper-pagination')
						},
						fadeEffect: {
							crossFade: true
						},

						...thumbs
					}
				})
			});
			productSlider.addClass('inited')
		}
	};

	window.reinit.slider.productSlider();
})