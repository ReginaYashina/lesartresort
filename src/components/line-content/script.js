window.addEventListener('load', function (event) {


	window.vendorLoader({
		name: 'magnific',
		path: '/js/vendor/jquery.magnific-popup.min.js',
		event: {
			scroll: true,
			timeout: true,
		},
		callback: () => {
			$('.js-video-play').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,
				iframe: {
					markup: '<div class="mfp-iframe-scaler">' +
						'<div class="mfp-close"></div>' +
						'<iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe>' +
						'</div>',
				}
			});
		}
	});
})