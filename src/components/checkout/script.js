window.addEventListener('load', function () {

	$('.js-form-submit').click(function(){
		$('.js-checkout-form').submit();
	})


	$('.js-mobile-basket').click(function(){
		
		if($(this).closest('.checkout-confirm__body').hasClass('active')){
			$(this).closest('.checkout-confirm__body').find('.basket-card').each(function(){
				$(this).slideUp();
			});
			$(this).closest('.checkout-confirm__body').removeClass('active');
		}else{
			$(this).closest('.checkout-confirm__body').find('.basket-card').each(function(){
				$(this).slideDown();
			});
			$(this).closest('.checkout-confirm__body').addClass('active');
		}



	})
})