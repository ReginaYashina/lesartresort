window.addEventListener('load', function (event) {
	window.vendorLoader({
		name: 'share',
		path: 'https://yastatic.net/share2/share.js',
		http: true,
		timeout: 3000,
		event: {
			scroll: true,
			timeout: true,
			mouseover: {
				trigger: '.ya-share2'
			}
		},
		callback: function () {
		}
	})

});
