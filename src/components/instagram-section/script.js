window.addEventListener('load', function () {

	if (!window.reinit) {
		window.reinit = {};
	}
	if (!window.reinit.slider) {
		window.reinit.slider = {};
	}

	window.reinit.slider.instagramSlider = function () {
		let instagramSlider = $('.js-slider-instagram')
		if (instagramSlider.length && !instagramSlider.hasClass('inited')) {
			instagramSlider.each(function (i, el) {


				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						autoHeight: false,
						lazy: true,
						spaceBetween: 6,
						watchOverflow: true,
						simulateTouch: true,
						centeredSlides: true,
						slidesPerView: 'auto',
						loop: length > 1,
						// effect: 'fade',
						pagination: {
							el: $(el).find('.js-swiper-pagination'),
							clickable: true
						},
						fadeEffect: {
							crossFade: true
						},
						on: {
							slideChange: function () {
							}
						},

					}
				})
					.on('init', () => {

					})
			});
			instagramSlider.addClass('inited')
		}
	};

	window.reinit.slider.instagramSlider();
})