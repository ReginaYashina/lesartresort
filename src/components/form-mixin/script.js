window.addEventListener('load', function (event) {

	$('.custom-select').each(function () {
		const select = $(this);
		select.click(function (ev) {
			ev.stopPropagation();
			$(this).toggleClass('active');
		})
		const selectInput = select.find('.custom-select__input');
		select.find('.custom-select__list-item').click(function (ev) {
			select.find('.custom-select__list-item').removeClass('selected');
			$(this).addClass('selected');
			const value = $(this).attr('data-value');
			const text = $(this).text();
			selectInput.val(value);
			select.find('.custom-select__selected').text(text);
			selectInput[0].dispatchEvent(new Event('change', { bubbles: true }));
		})
	})

	$(document).click(function () {
		$('.custom-select').removeClass('active');
	})

	$('.form-control').focus(function () {
		$(this).closest('.form-group').addClass('active');
	})

	$('.form-control').focusout(function () {
		if ($(this).val() || $(this).attr('type') == 'tel') return;
		$(this).closest('.form-group').removeClass('active');
	})

});