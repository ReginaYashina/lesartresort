window.addEventListener('load', function () {

	const options = {
		rootMargin: '0px',
		threshold: 0.25
	}

	$('.youtube-section').each(function(index){
		const observer = new IntersectionObserver((entries, observer) => {
			if (entries[0].isIntersecting) {
				$(this).find('.lazy-youtube').each(function () {
					if (!$(this).hasClass('inited')) {
						$(this).attr('src', $(this).attr('data-src'));
						$(this).addClass('inited');
					}
				})
			}
		}, options);

		observer.observe(this);
	})






	if (!window.reinit) {
		window.reinit = {};
	}
	if (!window.reinit.slider) {
		window.reinit.slider = {};
	}

	window.reinit.slider.youtubeSlider = function () {
		let youtubeSlider = $('.js-slider-youtube')
		if (youtubeSlider.length && !youtubeSlider.hasClass('inited')) {
			youtubeSlider.each(function (i, el) {


				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						autoHeight: false,
						lazy: true,
						spaceBetween: 0,
						watchOverflow: true,
						simulateTouch: true,
						slidesPerView: 1,
						loop: length > 1,
						// effect: 'fade',
						pagination: {
							el: $(el).find('.js-swiper-pagination'),
							clickable: true
						},
						fadeEffect: {
							crossFade: true
						},
						on: {
							slideChange: function () {
							}
						},

					}
				})
					.on('init', () => {

					})
			});
			youtubeSlider.addClass('inited')
		}
	};

	window.reinit.slider.youtubeSlider();


	window.reinit.slider.youtubeSliderLine = function () {
		let youtubeSliderLine = $('.js-slider-youtube-line')
		if (youtubeSliderLine.length && !youtubeSliderLine.hasClass('inited')) {
			youtubeSliderLine.each(function (i, el) {


				let slider = el;
				let length = slider.querySelectorAll('.swiper-slide').length;
				let swiper = window.slam_slider({
					el: el,
					args: {
						// autoHeight: false,
						lazy: true,
						spaceBetween: 0,
						watchOverflow: true,
						simulateTouch: true,
						slidesPerView: 'auto',
						loop: length > 1,
						// effect: 'fade',
						pagination: {
							el: $(el).find('.js-swiper-pagination'),
							clickable: true
						},
						fadeEffect: {
							crossFade: true
						},
						on: {
							slideChange: function () {
							}
						},

					}
				})
					.on('init', () => {

					})
			});
			youtubeSliderLine.addClass('inited')
		}
	};

	window.reinit.slider.youtubeSliderLine();
})