window.addEventListener('load', function () {

	//scrollWidth

	if ($('html').hasClass('custom-scroll')) {
		document.documentElement.style.setProperty('--scrollWidth', 8 + 'px');
	} else {
		(function () {
			let div = document.createElement('div');

			div.style.overflowY = 'scroll';
			div.style.width = '50px';
			div.style.height = '50px';

			// мы должны вставить элемент в документ, иначе размеры будут равны 0
			document.body.append(div);
			let scrollWidth = div.offsetWidth - div.clientWidth;
			div.remove();
			document.documentElement.style.setProperty('--scrollWidth', scrollWidth + 'px');
		})();
	}




	const scrollHandler = () => {
		if ($('.header-position-detect')[0].getBoundingClientRect().y < 0) {
			$('.header').addClass('header-sticky');
		} else {
			$('.header').removeClass('header-sticky');
		}
	};

	document.addEventListener('scroll', scrollHandler);


	$('.js-mobile-burger').click(function () {
		$('.header__mobile-menu-wrap').addClass('active');
	})

	$('.js-mobile-close').click(function () {
		$('.header__mobile-menu-wrap').removeClass('active');
	})

	$('.js-header-basket').click(function (ev) {
		ev.stopPropagation();
		$('.basket-mini').addClass('active');
		$('body').addClass('menu-opened');
	})

	$('.js-header-basket-close').click(function () {
		$('.basket-mini').removeClass('active');
		$('body').removeClass('menu-opened');
	})

	$(document).click(function (ev) {
		if ($(ev.target).closest('.basket-mini__inner').length || $(ev.target).hasClass('.basket-mini__inner')) return;
		$('.basket-mini').removeClass('active');
		$('body').removeClass('menu-opened');
	})

})


//alert
$('.js-close-alert').on('click touch', function () {
	$('.js-top-alert').slideUp();
});